<?php 
session_start();
require_once("vendor/autoload.php");

use \Slim\Slim;
use \EQuestions\Page;
use \EQuestions\PageAdmin;
use \EQuestions\Model\User;
use \EQuestions\Model\Question;
use \EQuestions\Model\Materia;
use \EQuestions\Model\Assunto;
use \EQuestions\Model\Prova;
use \EQuestions\Model\Banca;
use \EQuestions\Model\Orgao;
use \EQuestions\DB\Sql;

$app = new Slim();

$app->config('debug', true);

$app->get('/', function() { //Rota principal

    $page = new Page(array("titulo"=>"Início"));
    
    $page->setTpl("index");
    
});

$app->get('/admin', function() { //Rota principal

    User::verifyLogin();
    
    $page = new PageAdmin(array("titulo"=>"Administração"));
    
    $page->setTpl("index");
    
});

//////////////////// ADMIN - LOGIN-LOGOUT /////////////////////////////////////

$app->get('/admin/login', function() { //Rota principal
    
    //User::verifyLogin();
    
    $page = new PageAdmin(["titulo"=>"Login", "header"=>false, "footer"=>false]);
    
    $page->setTpl("login");
    
});

$app->post('/admin/login', function(){
    
    User::login($_POST["login"], $_POST['password']);
    
    header ("Location: /admin");
    exit;
     
});

$app->get('/admin/logout', function(){
    
    User::logout();
    
    header ("Location: /admin/login"); 
    exit;
    
});
//////////////////// ADMIN - USERS /////////////////////////////////////

//GET PARA CHAMAR PÁGINA DE EXIBIR USUÁRIOS
$app->get('/admin/users', function(){
    
    User::verifyLogin();
    
    $users = User::listAll();
    
    $page = new PageAdmin(array("titulo"=>"Usuários"));
    
    $page->setTpl("users", array(
        "users"=>$users
    ));
    
});
//GET PARA DELETAR USUÁRIO
$app->get('/admin/users/:iduser/delete', function($iduser){
    
    User::verifyLogin();
    
    $user = new User();
    
    $user->get((int)$iduser);
    
    $user->delete();
    
    header("Location: /admin/users");
    exit;

});

//GET PARA CHAMAR PÁGINA DE CADASTRO DE USUÁRIO
$app->get('/admin/users/create', function(){
    
    User::verifyLogin();
    
    $page = new PageAdmin(array("titulo"=>"Adicionar usuário"));
    
    $page->setTpl("users-create");

    
});

//POST PARA CRIAR USUÁRIO
$app->post('/admin/users/create', function(){ 
    
    User::verifyLogin();
    
    $user = new User();
    
    $_POST["inadmin"] = (isset($_POST["inadmin"]))?1:0;
    $_POST["despassword"] = password_hash($_POST["despassword"], PASSWORD_DEFAULT, ["cost"=>12]);
    $user->setData($_POST);
    
    $user->save();
    
    header ("Location: /admin/users");
    exit;
    
});

//GET PARA PAGINA DE UPDATE
$app->get('/admin/users/:iduser', function($iduser){ 
    
    User::verifyLogin();
        
    $user = new User();
    
    $user->get((int)$iduser);
    
    $page = new PageAdmin(array("titulo"=>"Atualizar usuário"));
    
    $page->setTpl("users-update", array(
        "user"=>$user->getValues()
    ));

});

//POST PARA UPDATE DE USUÁRIO
$app->post('/admin/users/:iduser', function($iduser){  
    
    User::verifyLogin();
    
    $user = new User();
    
    $_POST["inadmin"] = (isset($_POST["inadmin"]))?1:0;
    
    $user->get((int)$iduser);
    
    $user->setData($_POST);
    
    $user->update();
    
    header ("Location: /admin/users");
    exit;
 
});

//////////////////// ADMIN - QUESTOES /////////////////////////////////////
//Chamar página exibir questões na página ADM
$app->get('/admin/questions', function(){
    
    User::verifyLogin();
    
    $Q = Question::listAll();
    
    $page = new PageAdmin(array("titulo"=>"Questões"));
    
    $page->setTpl("questions", array(
        "questoes"=>$Q
    ));
    
});

//GET PARA DELETAR QUESTAO
$app->get('/admin/questions/:idquestao/delete', function($questao_id){
    
    User::verifyLogin();
    
    $questao = new Question();
    
    $questao->get((int)$questao_id);
    
    $questao->delete();
    
    header("Location: /admin/questions");
    exit;

});


//Chamar página para criar questão
$app->get('/admin/questions/create', function(){ 
    
    User::verifyLogin();
    
    $prova = Prova::listAll();
    $assunto = Assunto::listAllAssunto_Materia_Qtd_Questoes();
    
    $page = new PageAdmin(array("titulo"=>"Adicionar questão"));
    
    $page->setTpl("questions-create", array("provas"=>$prova, "assuntos"=>$assunto));
    
});

//POST PARA CRIAR QUESTAO
$app->post('/admin/questions/create', function(){ 
    
    User::verifyLogin();
    
    $questao = new Question();
        
    $questao->setData($_POST);
    
    $questao->save();
    
    header ("Location: /admin/questions");
    exit;
    
});

//GET PARA PAGINA DE UPDATE DE QUESTOES
$app->get('/admin/questions/:idquestao', function($questao_id){ 
    
    User::verifyLogin();
        
    $questao = new Question();
    
    $questao->get((int)$questao_id);
    $prova = Prova::listAll();
    $assunto = Assunto::listAllAssunto_Materia_Qtd_Questoes();
    
    $page = new PageAdmin(array("titulo"=>"Atualizar questão"));
    
    $page->setTpl("questions-update", array(
        "question"=>$questao->getValues(), "provas"=>$prova, "assuntos"=>$assunto
    ));

});

//POST PARA UPDATE DE QUESTAO
$app->post('/admin/questions/:idquestao', function($questao_id){  
    
    User::verifyLogin();
    
    $questao = new Question();
    
    $questao->get((int)$questao_id);
    
    $questao->setData($_POST);
    
    $questao->update();
    
    header ("Location: /admin/questions");
    exit;
 
});


//////////////////// ADMIN - SENHA /////////////////////////////////////
//TELA DE ESQUECI A SENHA
$app->get('/admin/forgot', function(){
                
    //User::verifyLogin();
    
    $page = new PageAdmin(["header"=>false, "footer"=>false]);
    
    $page->setTpl("forgot");
    
});

//POST DE ESQUECI A SENHA
$app->post('/admin/forgot', function(){

    $user = User::getForgot($_POST["email"]);

    header("Location: /admin/forgot/sent");
    exit;
    
});

//GET EMAIL ENVIADO
$app->get('/admin/forgot/sent', function(){

    $page = new PageAdmin(["header"=>false, "footer"=>false]);
    
    $page->setTpl("forgot-sent");
    
});

//GET PARA RESETAR A SENHA
$app->get('/admin/forgot/reset', function(){
    
    $user = User::validForgotDecrypt($_GET["code"]);
    
    $page = new PageAdmin(["header"=>false, "footer"=>false]);
    
    $page->setTpl("forgot-reset", array(
    "name"=>$user["desperson"],
    "code"=>$_GET["code"]));
    
    
});

$app->post('/admin/forgot/reset', function(){
    
    $forgot = User::validForgotDecrypt($_POST["code"]);
    
    User::setForgotUsed($forgot["idrecovery"]);
    
    $user = new User();
    
    $user->get((int)$forgot["iduser"]);
    
    $password = password_hash($_POST["password"], PASSWORD_DEFAULT, ["cost"=>12]);
    
    $user->setPassword($password);
    
    $page = new PageAdmin(["header"=>false, "footer"=>false]);
    
    $page->setTpl("forgot-reset-success");
    
});

/////////////////// ADMIN - MATÉRIA ////////////////////

//GET PARA CHAMAR PÁGINA DE EXIBIR MATÉRIAS
$app->get('/admin/materias', function(){
    
    User::verifyLogin();
    
    $materia = Materia::listAll();
    
    $page = new PageAdmin(array("titulo"=>"Matérias"));
    
    $page->setTpl("materias", array(
        "materias"=>$materia
    ));
    
});
//GET PARA DELETAR MATÉRIA
$app->get('/admin/materias/:idmateria/delete', function($materia_id){ 
    
    User::verifyLogin();
    
    $materia = new Materia();
    
    $materia->get((int)$materia_id);
    
    $materia->delete();
    
    header("Location: /admin/materias");
    exit;

});

//GET PARA CHAMAR PÁGINA DE CADASTRO DE MATÉRIA
$app->get('/admin/materias/create', function(){
    
    User::verifyLogin();
    
    $page = new PageAdmin(array("titulo"=>"Adicionar matéria"));
    
    $page->setTpl("materias-create");

    
});

//POST PARA CRIAR MATÉRIA
$app->post('/admin/materias/create', function(){ 
    
    User::verifyLogin();
    
    $materia = new Materia();
    
    $materia->setData($_POST);
        
    $materia->save();
    
    header ("Location: /admin/materias");
    exit;
    
});

//GET PARA PAGINA DE MATÉRIA
$app->get('/admin/materias/:idmateria', function($materia_id){ 
    
    User::verifyLogin();
        
    $materia = new Materia();
    
    $materia->get((int)$materia_id);
    
    $page = new PageAdmin(array("titulo"=>"Atualizar matéria"));
    
    $page->setTpl("materias-update", array(
        "materia"=>$materia->getValues()
    ));

});

//POST PARA UPDATE DE MATÉRIA
$app->post('/admin/materias/:idmateria', function($materia_id){  
    
    User::verifyLogin();
    
    $materia = new Materia();
    
    $materia->get((int)$materia_id);
    
    $materia->setData($_POST);
    
    $materia->save();
    
    header ("Location: /admin/materias");
    exit;
 
});



//////////////////// ADMIN - ASSUNTOS /////////////////
//GET PARA CHAMAR PÁGINA DE EXIBIR ASSUNTOS
$app->get('/admin/assuntos', function(){
    
    User::verifyLogin();
    
    $assunto = Assunto::listAllAssunto_Materia_Qtd_Questoes();
    
    $page = new PageAdmin(array("titulo"=>"Assuntos"));
    
    $page->setTpl("assuntos", array(
        "assuntos"=>$assunto
    ));
    
});
//GET PARA DELETAR ASSUNTOS
$app->get('/admin/assuntos/:idmateria/delete', function($assunto_id){ 
    
    User::verifyLogin();
    
    $assunto = new Assunto();
    
    $assunto->get((int)$assunto_id);
    
    $assunto->delete();
    
    header("Location: /admin/assuntos");
    exit;

});

//GET PARA CHAMAR PÁGINA DE CADASTRO DE ASSUNTOS
$app->get('/admin/assuntos/create', function(){
    
    User::verifyLogin();
    
    $page = new PageAdmin(array("titulo"=>"Adicionar assunto"));
    
    $materia = Materia::listAll();
    
    $page->setTpl("assuntos-create", array(
        "materias"=>$materia
    ));

    
});

//POST PARA CRIAR ASSUNTO
$app->post('/admin/assuntos/create', function(){ 
    
    User::verifyLogin();
    
    $assunto = new Assunto();
    
    $assunto->setData($_POST);
    
    $assunto->save();
    
    header ("Location: /admin/assuntos");
    exit;
    
});

//GET PARA PAGINA DE ASSUNTO
$app->get('/admin/assuntos/:idassunto', function($assunto_id){ 
    
    User::verifyLogin();
        
    $assunto = new Assunto();
    
    $assunto->get((int)$assunto_id);
    
    $materia = Materia::listAll(); //FALTA OLHAR
    
    $page = new PageAdmin(array("titulo"=>"Atualizar assunto"));
    
    $page->setTpl("assuntos-update", array(
        "assunto"=>$assunto->getValues(),
        "materias"=>$materia
    ));

});

//POST PARA UPDATE DE MATÉRIA
$app->post('/admin/assuntos/:idassunto', function($assunto_id){  
    
    User::verifyLogin();
    
    $assunto = new Assunto();
    
    $assunto->get((int)$assunto_id);
    
    $assunto->setData($_POST);
    
    $assunto->save();
    
    header ("Location: /admin/assuntos");
    exit;
 
});



//////////////////// ADMIN - PROVAS /////////////////
//GET PARA CHAMAR PÁGINA DE EXIBIR PROVA
$app->get('/admin/provas', function(){
    
    User::verifyLogin();
    
    $prova = Prova::listAll();
    
    $page = new PageAdmin(array("titulo"=>"Provas"));
    
    $page->setTpl("provas", array(
        "provas"=>$prova
    ));
    
});
//GET PARA DELETAR PROVA
$app->get('/admin/provas/:prova_id/delete', function($prova_id){ 
    
    User::verifyLogin();
    
    $prova = new Prova();
    
    $prova->get((int)$prova_id);
    
    $prova->delete();
    
    header("Location: /admin/provas");
    exit;

});

//GET PARA CHAMAR PÁGINA DE CADASTRO DE PROVAS
$app->get('/admin/provas/create', function(){
    
    User::verifyLogin();
    
    $page = new PageAdmin(array("titulo"=>"Adicionar prova"));
    
    $orgao = Orgao::listAll();
    $banca = Banca::listAll();
    
    $page->setTpl("provas-create", array(
        "orgaos"=>$orgao,
        "bancas"=>$banca
    ));

    
});

//POST PARA CRIAR PROVA
$app->post('/admin/provas/create', function(){ 
    
    User::verifyLogin();
    
    $prova = new Prova();
    
    $prova->setData($_POST);
    
    var_dump($prova);
    $prova->save();
    
    header ("Location: /admin/provas");
    exit;
    
});

//GET PARA UPDATE DE PROVA
$app->get('/admin/provas/:prova_id', function($prova_id){ 
    
    User::verifyLogin();
        
    $prova = new Prova();
    
    $prova->get((int)$prova_id);
    
    $orgao = Orgao::listAll();
    $banca = Banca::listAll();
    
    $page = new PageAdmin(array("titulo"=>"Atualizar prova"));
    
    $page->setTpl("provas-update", array(
        "prova"=>$prova->getValues(),
        "orgaos"=>$orgao,
        "bancas"=>$banca
    ));

});

//POST PARA UPDATE DE PROVA
$app->post('/admin/provas/:prova_id', function($prova_id){  
    
    User::verifyLogin();
    
    $prova = new Prova();
    
    $prova->get((int)$prova_id);
    
    $prova->setData($_POST);
    
    $prova->save();
    
    header ("Location: /admin/provas");
    exit;
 
});


//////////////////// ADMIN - BANCAS /////////////////
//GET PARA CHAMAR PÁGINA DE EXIBIR BANCA
$app->get('/admin/bancas', function(){
    
    User::verifyLogin();
    
    $banca = Banca::listAll();
    
    $page = new PageAdmin(array("titulo"=>"Banca"));
    
    $page->setTpl("bancas", array(
        "bancas"=>$banca
    ));
    
});
//GET PARA DELETAR BANCA
$app->get('/admin/bancas/:banca_id/delete', function($banca_id){ 
    
    User::verifyLogin();
    
    $banca = new Banca();
    
    $banca->get((int)$banca_id);
    
    $banca->delete();
    
    header("Location: /admin/bancas");
    exit;

});

//GET PARA CHAMAR PÁGINA DE CADASTRO DE BANCAS
$app->get('/admin/bancas/create', function(){
    
    User::verifyLogin();
    
    $page = new PageAdmin(array("titulo"=>"Adicionar banca"));
    
    $page->setTpl("bancas-create");

    
});

//POST PARA CRIAR BANCA
$app->post('/admin/bancas/create', function(){ 
    
    User::verifyLogin();
    
    $banca = new Banca();
    
    $banca->setData($_POST);
    
    $banca->save();
    
    header ("Location: /admin/bancas");
    exit;
    
});

//GET PARA UPDATE DE BANCA
$app->get('/admin/bancas/:banca_id', function($banca_id){ 
    
    User::verifyLogin();
        
    $banca = new Banca();
    
    $banca->get((int)$banca_id);
        
    $page = new PageAdmin(array("titulo"=>"Atualizar banca"));
    
    $page->setTpl("bancas-update", array(
        "bancas"=>$banca->getValues()

    ));

});

//POST PARA UPDATE DE BANCA
$app->post('/admin/bancas/:banca_id', function($banca_id){  
    
    User::verifyLogin();
    
    $banca = new Banca();
    
    $banca->get((int)$banca_id);
    
    $banca->setData($_POST);
    
    $banca->save();
    
    header ("Location: /admin/bancas");
    exit;
 
});

//////////////////// ADMIN - ÓRGÃO /////////////////
//GET PARA CHAMAR PÁGINA DE EXIBIR ÓRGÃO
$app->get('/admin/orgaos', function(){
    
    User::verifyLogin();
    
    $orgao = Orgao::listAll();
    
    $page = new PageAdmin(array("titulo"=>"Órgãos"));
    
    $page->setTpl("orgaos", array(
        "orgaos"=>$orgao
    ));
    
});
//GET PARA DELETAR BANCA
$app->get('/admin/orgaos/:orgao_id/delete', function($orgao_id){ 
    
    User::verifyLogin();
    
    $orgao = new Orgao();
    
    $orgao->get((int)$orgao_id);
    
    $orgao->delete();
    
    header("Location: /admin/orgaos");
    exit;

});

//GET PARA CHAMAR PÁGINA DE CADASTRO DE ÓRGÃOS
$app->get('/admin/orgaos/create', function(){
    
    User::verifyLogin();
    
    $page = new PageAdmin(array("titulo"=>"Adicionar órgão"));
    
    $page->setTpl("orgaos-create");

    
});

//POST PARA CRIAR ÓRGÃO
$app->post('/admin/orgaos/create', function(){ 
    
    User::verifyLogin();
    
    $orgao = new Orgao();
    
    $orgao->setData($_POST);
    
    $orgao->save();
    
    header ("Location: /admin/orgaos");
    exit;
    
});

//GET PARA UPDATE DE ÓRGÃO
$app->get('/admin/orgaos/:orgao_id', function($orgao_id){ 
    
    User::verifyLogin();
        
    $orgao = new Orgao();
    
    $orgao->get((int)$orgao_id);
        
    $page = new PageAdmin(array("titulo"=>"Atualizar órgão"));
    
    $page->setTpl("orgaos-update", array(
        "orgao"=>$orgao->getValues()

    ));

});

//POST PARA UPDATE DE ÓRGÃO
$app->post('/admin/orgaos/:orgao_id', function($orgao_id){  
    
    User::verifyLogin();
    
    $orgao = new Orgao();
    
    $orgao->get((int)$orgao_id);
    
    $orgao->setData($_POST);
    
    $orgao->save();
    
    header ("Location: /admin/orgaos");
    exit;
 
});


////////////QUESTOES - SITE///////////
//Exibir questões!
$app->get('/questions', function(){
    
    $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
        
    $pagination = Question::listAllFormatadoPage($page);
    $first = 1;
    $last = $pagination['pages'];
    $pages = [];
    
    if ($page <= 4){
        $i = 1;
        $max = $page + 4;
    } else {
        $i = $page-4;
        if ($page >= $pagination['pages']-4){
            $max = $pagination['pages'];
        } else {
            $max = $i + 8;
        }
    }
    
    
    for($i; $i <= $max; $i++){
        array_push($pages, [
            'link'=>'/questions?page='.$i,
            'page'=>$i
            
        ]);
    };
    
    
    
    $page = new Page(array("titulo"=>"Questões"));
    
    $page->setTpl("questions", array(
        "questoes"=>$pagination['data'],
        "pages"=>$pages
    ));
    
});

///////////////////// PROVAS - SITE //////////////////////////
$app->get('/provas', function(){ //Exibir provas!
                
    $lista_de_provas = Prova::listAllOnlyWithProvas();
    
    $page = new Page(array("titulo"=>"Provas"));
    
    $page->setTpl("provas", array(
        "provas"=>$lista_de_provas
    ));
    
});

$app->get('/provas/:id', function($prova_id){
                
    $questoes_da_prova = Prova::listAllforProva($prova_id);
    
    $prova = new Prova();
    
    $prova->get((int)$prova_id);
    
    $page = new Page(array("titulo"=>"Provas"));
    
    $page->setTpl("questions_prova", array(
        "questoes"=>$questoes_da_prova,
        "cargo"=>$prova->getcargo(),
        "ano"=>$prova->getano(),
        "nivel"=>$prova->getnivel(),
        "orgao"=>$prova->getorgao(),
        "banca"=>$prova->getbanca(),
        "banca_sigla"=>$prova->getbanca_sigla(),
        "orgao_sigla"=>$prova->getorgao_sigla(),
    ));
    
});

///////////////////// MATÉRIAS FOOTER - SITE //////////////////////////
$app->get('/materias/:id', function($materia_id){
    
    $materia = new Materia();
    
    $materia->get((int)$materia_id);
                
    $questoes_da_materia = Question::listAllFormatadoPorMateria((int)$materia_id);
    
    $page = new Page(array("titulo"=>"Questões de ".$materia->getnome().""));
    
    $page->setTpl("questions", array(
        "questoes"=>$questoes_da_materia
    ));
    
});

///////////////////// MATÉRIAS FOOTER - SITE //////////////////////////
$app->get('/materias', function(){
    
    $page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
    
    $pagination = Materia::listAllcomLimitPage($page);
    
    $pages = [];
    
    for($i=1; $i <= $pagination['pages']; $i++){
        array_push($pages, [
            'link'=>'/materias?page='.$i,
            'page'=>$i
            
        ]);
    };
    
    $page = new Page(array("titulo"=>"Matérias"));
    
    
    $page->setTpl("materias", array(
        "materias"=>$pagination['data'],
        "pages"=>$pages
    ));
    
});

/////////////////////MATÉRIAS //////////////////////////

$app->run();

 ?>