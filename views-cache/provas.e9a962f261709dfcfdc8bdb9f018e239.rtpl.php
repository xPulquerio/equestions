<?php if(!class_exists('Rain\Tpl')){exit;}?>    <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="latest-product">
                        <h2 class="titulo_meu">Provas</h2>
                        <style>
                            .form_provas_completo {
                                border: 2px solid black;
                            }
                            .table_full tr td{
                                width: 50%;
                                padding: 3px 5px 3px 5px;
                            }
                            table {
                                
                                width: 100%;
                            }
                            .meu_button {
                                border-radius: 10px;
                                width: 200px;
                                color: black;
                                background-color: aliceblue;
                            }
                            ul {
                                list-style: none;
                            }
                            
                        </style>
                        <?php $cont = 1;; ?>
                        <?php $counter1=-1;  if( isset($provas) && ( is_array($provas) || $provas instanceof Traversable ) && sizeof($provas) ) foreach( $provas as $key1 => $value1 ){ $counter1++; ?>
                            <form class="form_provas_completo" action="/provas/<?php echo $value1["id"]; ?>" method="get">
                                <table class="table_full">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td><b><?php echo $cont++; ?></b> </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Banca:</b> <?php echo $value1["banca_sigla"]; ?> - <?php echo $value1["banca"]; ?> </td>
                                                            </tr>
                                                            <tr>    
                                                                <td><b>Ano:</b> <?php echo $value1["ano"]; ?> </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Cargo:</b> <?php echo $value1["cargo"]; ?> </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Órgão:</b> <?php echo $value1["orgao"]; ?> - (<?php echo $value1["orgao_sigla"]; ?>) </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Nível:</b> <?php echo $value1["nivel"]; ?> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="text-align: right"><b><?php echo $value1["Qtd"]; ?></b> <i>questões nesta prova!</i> </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="text-align: center;">
                                            <ul>
                                                <li><input class="meu_button" type='submit' value='Questoes'></li>
                                                <!--<li><button><a href="/provas/{value.id}"></a></button></li>-->
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <br>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End main content area -->