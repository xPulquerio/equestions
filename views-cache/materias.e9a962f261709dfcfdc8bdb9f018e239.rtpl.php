<?php if(!class_exists('Rain\Tpl')){exit;}?>    <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="latest-product">
                        <h2 class="titulo_meu">Matérias</h2>
                        <style>
                            .form_provas_completo {
                                border: 2px solid black;
                            }
                            .table_full tr td{
                                width: 50%;
                                padding: 3px 5px 3px 5px;
                            }
                            table {
                                
                                width: 100%;
                            }
                            .meu_button {
                                border-radius: 10px;
                                width: 200px;
                                color: black;
                                background-color: aliceblue;
                            }
                            ul {
                                list-style: none;
                            }
                            
                        </style>
                        <?php $counter1=-1;  if( isset($materias) && ( is_array($materias) || $materias instanceof Traversable ) && sizeof($materias) ) foreach( $materias as $key1 => $value1 ){ $counter1++; ?>
                            <form class="form_provas_completo" action="/materias/<?php echo $value1["id"]; ?>" method="get">
                                <table class="table_full">
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td><b><?php echo $value1["id"]; ?></b> </td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Nome:</b> <?php echo $value1["nome"]; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><b>Quantidade:</b> <?php echo $value1["Qtd"]; ?></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="text-align: center;">
                                            <ul>
                                                <li><input class="meu_button" type='submit' value='Questoes'></li>
                                                <!--<li><button><a href="/provas/{value.id}"></a></button></li>-->
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <br>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End main content area -->

    <div class="row">
            <div class="col-md-12">
                <div class="product-pagination text-center">
                    <nav>
                        <ul class="pagination">
                            <?php $counter1=-1;  if( isset($pages) && ( is_array($pages) || $pages instanceof Traversable ) && sizeof($pages) ) foreach( $pages as $key1 => $value1 ){ $counter1++; ?>
                            <li><a href="<?php echo $value1["link"]; ?>"><?php echo $value1["page"]; ?></a></li>
                            <?php } ?>
                        </ul>
                    </nav>                        
                </div>
            </div>
        </div>