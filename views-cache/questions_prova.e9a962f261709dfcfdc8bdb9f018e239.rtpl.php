<?php if(!class_exists('Rain\Tpl')){exit;}?>    <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="latest-product">
                        <h4 class="titulo_meu">Questões da Prova para <b><?php echo $cargo; ?></b></h4>
                        <h4>Órgão: <u><?php echo $orgao; ?></u> - <?php echo $orgao_sigla; ?></h4>
                        <h4>Ano: <?php echo $ano; ?></h4>
                        <style>
                            table .header_da_questao tr td{
                                background-color: darkgray;
                                border: 1px solid black;
                                padding: 3px 5px 3px 5px;
                            }
                            table {
                                width: 100%;
                            }
                        </style>
                        <?php $cont = 1;; ?>
                        
                        <?php $counter1=-1;  if( isset($questoes) && ( is_array($questoes) || $questoes instanceof Traversable ) && sizeof($questoes) ) foreach( $questoes as $key1 => $value1 ){ $counter1++; ?>
                            <form class="questao_completa" method="POST" target="<?php echo $value1["id"]; ?>" action="/resposta.php">
                                <table>
                                    <tr>
                                        <td>
                                            <table class="header_da_questao">
                                                <tr>
                                                    <td><b><?php echo $cont++; ?></b></td>
                                                    <td>Cod.<?php echo $value1["id"]; ?> </td>
                                                    <td>Banca: <?php echo $banca_sigla; ?> </td>
                                                    <td>Ano: <?php echo $ano; ?> </td>
                                                    <td>Cargo: <?php echo $cargo; ?> </td>
                                                    <td>Assunto: <?php echo $value1["assunto"]; ?> </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: justify;"><?php echo $value1["texto"]; ?></td>
                                    </tr>
                                    <?php if( $value1["img_url"] != NULL ){ ?>
                                    <tr>
                                        <td><img src="/res/site/img/<?php echo $value1["img_url"]; ?>"/><br><br></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td><?php echo $value1["pergunta"]; ?></td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <label><input id='alternativa1' type='radio' name='gender' value='alternativa1'> <?php echo $value1["alternativa1"]; ?></label> <!--id='alternativa1' for='alternativa1'-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label><input id='alternativa2' type='radio' name='gender' value='alternativa2'> <?php echo $value1["alternativa2"]; ?></label>
                                        </td>
                                    </tr>
                                    <?php if( $value1["alternativa3"] != NULL ){ ?>
                                    <tr>
                                        <td>
                                            <label><input id='alternativa3' type='radio' name='gender' value='alternativa3'> <?php echo $value1["alternativa3"]; ?></label>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <?php if( $value1["alternativa4"] != NULL ){ ?>
                                    <tr>
                                        <td>
                                            <label><input id='alternativa4' type='radio' name='gender' value='alternativa4'> <?php echo $value1["alternativa4"]; ?></label>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <?php if( $value1["alternativa5"] != NULL ){ ?>
                                    <tr>
                                        <td>
                                            <label><input id='alternativa5' type='radio' name='gender' value='alternativa5'> <?php echo $value1["alternativa5"]; ?></label>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                                <input type='hidden' name='resolucao' value='<?php echo $value1["resolucao"]; ?>'/>
                                <input type='hidden' name='id' value='<?php echo $value1["id"]; ?>'/>
                                <input type='hidden' name='correta' value='<?php echo $value1["correta"]; ?>'/>
                                <input type='submit' value='Responder'>
                                <input type='reset' value='Resetar'>
                            </form>
                            <br>
                            <iframe class="frame_pergunta" name="<?php echo $value1["id"]; ?>"></iframe>
                            <br>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End main content area -->