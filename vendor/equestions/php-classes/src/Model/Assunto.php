<?php 

namespace EQuestions\Model;

use \EQuestions\Model;
use \EQuestions\DB\Sql;
use \EQuestions\Mailer;

class Assunto extends Model {
        
    protected $fields = [
		"id", "nome", "materia_id", "qtd_questoes_por_assunto"
	];
    
    public static function listAllAssunto_Materia_Qtd_Questoes(){
        
        $sql = new Sql();
        
        return $sql->select("SELECT tb_assunto.id, tb_assunto.nome AS nome, tb_materia.nome as materia, count(tb_questao_assunto.id) as qtd_questoes from tb_questao
inner join tb_questao_assunto on tb_questao.id = tb_questao_assunto.questao_id
right join tb_assunto on tb_assunto.id = tb_questao_assunto.assunto_id
inner join tb_materia on tb_assunto.materia_id = tb_materia.id
group by tb_assunto.id ORDER BY tb_materia.nome ASC, tb_assunto.nome ASC");
        
    }
    
    public function save(){
        
        $sql = new Sql();
        
        $r = $sql->select("CALL sp_assuntos_save(:id, :nome, :materia_id)", array(
            ":id"=>$this->getid(),
            ":nome"=>$this->getnome(),
            ":materia_id"=>$this->getmateria_id()
        ));
          
        $this->setData($r[0]);
    }
    
    public function get($assunto_id){
        
        $sql = new Sql();
        
        $results = $sql->select("SELECT * FROM tb_assunto WHERE id = :id", array(
        
        ":id"=>$assunto_id
        
        ));
        
        $this->setData($results[0]);
        
    }
    
    public function delete(){ //falta implementar
        
        $sql = new Sql();
        
        $sql->query("DELETE FROM tb_assunto WHERE id = :id", array(
        
            ":id"=>$this->getid()
        
        ));
        
    }
    
    public function getQuestoesPorAssunto(){
        
        $sql = new Sql();
        
        return $sql->select("SELECT count(*) as qtd from tb_questao_assunto WHERE assunto_id = :id group by assunto_id",array(":id"=>$this->getid()
                                                                                                       ));
        
    } 
        
}

 ?>