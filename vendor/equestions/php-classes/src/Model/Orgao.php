<?php 

namespace EQuestions\Model;

use \EQuestions\Model;
use \EQuestions\DB\Sql;
use \EQuestions\Mailer;

class Orgao extends Model {
        
    protected $fields = [
		"id", "nome", "sigla"
	];
    
    public static function listAll(){
        
        $sql = new Sql();
        
        return $sql->select("SELECT * FROM tb_orgao ORDER BY nome ASC");
        
    }
    
    public function save(){
        
        if($this->getsigla() == ''){
            $this->settexto(NULL);
        }
        
        
        $sql = new Sql();
        
        $r = $sql->select("CALL sp_orgaos_save(:id, :nome, :sigla)", array(
            ":id"=>$this->getid(),
            ":nome"=>$this->getnome(),
            ":sigla"=>$this->getsigla()
        ));
          
        $this->setData($r[0]);
    }
    
    public function get($orgao_id){
        
        $sql = new Sql();
        
        $results = $sql->select("SELECT * FROM tb_orgao WHERE id = :id", array(
        
        ":id"=>$orgao_id
        
        ));
        
        $this->setData($results[0]);
        
    }
    
    public function delete(){
        
        $sql = new Sql();
        
        $sql->query("DELETE FROM tb_orgao WHERE id = :id", array(
        
            ":id"=>$this->getid()
        
        ));
        
    }
        
}

 ?>