<?php 

namespace EQuestions\Model;

use \EQuestions\Model;
use \EQuestions\DB\Sql;
use \EQuestions\Mailer;

class Materia extends Model {
        
    protected $fields = [
		"id", "nome"
	];
    
    public static function listAllcomLimit($x){
        
        $sql = new Sql();
        
        return $sql->select("SELECT T.id as id, T.nome as nome, count(T.id) as Qtd from (SELECT tb_materia.* FROM tb_materia 
inner join tb_assunto on tb_materia.id = tb_assunto.materia_id
inner join tb_questao_assunto on tb_questao_assunto.assunto_id = tb_assunto.id
inner join tb_questao on tb_questao.id = tb_questao_assunto.questao_id group by tb_questao.id) as
T group by T.id ORDER BY Qtd DESC LIMIT $x"); //Quantidade de questões por matéria ordenado por mais matérias.
        
    }
    
    public static function listAllcomLimitPage($page = 1, $itensPerPage = 10){
        
        $start = ($page - 1) * $itensPerPage;
        
        $sql = new Sql();
        
        $results = $sql->select("SELECT SQL_CALC_FOUND_ROWS T.id as id, T.nome as nome, count(T.id) as Qtd from (SELECT tb_materia.* FROM tb_materia 
inner join tb_assunto on tb_materia.id = tb_assunto.materia_id
inner join tb_questao_assunto on tb_questao_assunto.assunto_id = tb_assunto.id
inner join tb_questao on tb_questao.id = tb_questao_assunto.questao_id group by tb_questao.id) as
T group by T.id ORDER BY Qtd DESC LIMIT $start, $itensPerPage;");
        
        $resultTotal = $sql->select("select FOUND_ROWS() AS nrtotal;");
        
        return [
            'data'=>$results,
            'total'=>$resultTotal[0]["nrtotal"],
            'pages'=>ceil($resultTotal[0]["nrtotal"] / $itensPerPage)
               ];
    }
    
    
    
    public static function listAll(){
        
        $sql = new Sql();
        
        return $sql->select("SELECT * FROM tb_materia ORDER BY nome ASC");
        
    }
    
    public function save(){
        
        $sql = new Sql();
        
        $r = $sql->select("CALL sp_materias_save(:id, :nome)", array(
           ":id"=>$this->getid(),
           ":nome"=>$this->getnome()
        ));
          
        $this->setData($r[0]);
        
        Materia::updateFile();
    }
    
    public function get($materia_id){
        
        $sql = new Sql();
        
        $results = $sql->select("SELECT * FROM tb_materia WHERE id = :id", array(
        
        ":id"=>$materia_id
        
        ));
        
        $this->setData($results[0]);
        
        
        
    }
    
    public function delete(){
        
        $sql = new Sql();
        
        $sql->query("DELETE FROM tb_materia WHERE id = :id", array(
        
            ":id"=>$this->getid()
        
        ));
        
        Materia::updateFile();
        
    }
    
    public static function updateFile(){
        
        $materias = Materia::listAllcomLimit(5);
        
        $html = [];
        
        foreach ($materias as $row){
            
            array_push($html, '<li><a href="/materias/'.$row['id'].'">'.$row['nome'].'</a></li>');
            
        }
        
        file_put_contents($_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "materias-menu.html", implode('',$html));
        
    }
        
}

 ?>