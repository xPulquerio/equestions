<?php 

namespace EQuestions\Model;

use \EQuestions\Model;
use \EQuestions\DB\Sql;

class Question extends Model {

	//const SESSION = "Question";

	protected $fields = [
		"id","legenda_da_imagem","img_url","texto", "pergunta", "alternativa1", "alternativa2", "alternativa3", "alternativa4", "alternativa5", "correta", "resolucao", "prova_id", "assunto_id"
	];

    
    public static function listAllFormatadoPorMateria($materia_id){
        
        $sql = new Sql();
        
        return $sql->select("SELECT T.*, tb_materia.nome as materia, tb_assunto.nome as assunto FROM (SELECT tb_questao.*, tb_prova.ano, tb_prova.nivel, tb_prova.cargo, tb_banca.sigla as banca, tb_orgao.nome as orgao FROM tb_questao INNER JOIN tb_prova ON tb_questao.prova_id = tb_prova.id INNER JOIN tb_banca on tb_prova.banca_id = tb_banca.id INNER JOIN tb_orgao on tb_prova.orgao_id = tb_orgao.id) T INNER JOIN tb_questao_assunto on T.id = tb_questao_assunto.questao_id INNER JOIN tb_assunto on tb_questao_assunto.assunto_id = tb_assunto.id INNER JOIN tb_materia on tb_assunto.materia_id = tb_materia.id WHERE tb_materia.id = :materia_id GROUP BY T.id ORDER BY rand()", array(
            ":materia_id"=>$materia_id
        ));
        
    }
    
    public static function listAllFormatado(){
        
        $sql = new Sql();
        
        return $sql->select("SELECT T.*, tb_materia.nome as materia, tb_assunto.nome as assunto FROM (SELECT tb_questao.*, tb_prova.ano, tb_prova.nivel, tb_prova.cargo, tb_banca.sigla as banca, tb_orgao.nome as orgao FROM tb_questao INNER JOIN tb_prova ON tb_questao.prova_id = tb_prova.id INNER JOIN tb_banca on tb_prova.banca_id = tb_banca.id INNER JOIN tb_orgao on tb_prova.orgao_id = tb_orgao.id) T INNER JOIN tb_questao_assunto on T.id = tb_questao_assunto.questao_id INNER JOIN tb_assunto on tb_questao_assunto.assunto_id = tb_assunto.id INNER JOIN tb_materia on tb_assunto.materia_id = tb_materia.id GROUP BY T.id ORDER BY rand()");
        
    }
    
    public static function listAllFormatadoPage($page = 1, $itensPerPage = 10){
        
        $start = ($page - 1) * $itensPerPage;
        
        $sql = new Sql();
        
        $results = $sql->select("SELECT SQL_CALC_FOUND_ROWS T.*, tb_materia.nome as materia, tb_assunto.nome as assunto FROM (SELECT tb_questao.*, tb_prova.ano, tb_prova.nivel, tb_prova.cargo, tb_banca.sigla as banca, tb_orgao.nome as orgao FROM tb_questao INNER JOIN tb_prova ON tb_questao.prova_id = tb_prova.id INNER JOIN tb_banca on tb_prova.banca_id = tb_banca.id INNER JOIN tb_orgao on tb_prova.orgao_id = tb_orgao.id) T INNER JOIN tb_questao_assunto on T.id = tb_questao_assunto.questao_id INNER JOIN tb_assunto on tb_questao_assunto.assunto_id = tb_assunto.id INNER JOIN tb_materia on tb_assunto.materia_id = tb_materia.id GROUP BY T.id ORDER BY id desc LIMIT $start, $itensPerPage;");
        
        $resultTotal = $sql->select("select FOUND_ROWS() AS nrtotal;");
        
        return [
            'data'=>$results,
            'total'=>$resultTotal[0]["nrtotal"],
            'pages'=>ceil($resultTotal[0]["nrtotal"] / $itensPerPage)
               ];
        
    }
    
    public static function listAll(){
        
        $sql = new Sql();
        
        return $sql->select("SELECT * FROM tb_questao ORDER BY id DESC");
        
    }
    
    
    public function save(){
        
        $sql = new Sql();
        
        /*
            legenda_da_imagem varchar(200), 
            img_url varchar(30),
            texto varchar(3000),
            pergunta varchar(1200),
            alternativa1 varchar(1000),
            alternativa2 varchar(1000),
            alternativa3 varchar(1000),
            alternativa4 varchar(1000),
            alternativa5 varchar(1000),
            correta varchar(200),
            resolucao varchar(3000) ,
            prova_id int(11),
            assunto_id INT
        */
        if($this->gettexto() == ''){
            $this->settexto(NULL);
        }
        
        if($this->getimg_url() == ''){
            $this->setimg_url(NULL);
        }

        if($this->getlegenda_da_imagem() == ''){
            $this->setlegenda_da_imagem(NULL);
        }

        if($this->getalternativa3() == ''){
            $this->setalternativa3(NULL);
        }
        if($this->getalternativa4() == ''){
            $this->setalternativa4(NULL);
        }

        if($this->getalternativa5() == ''){
            $this->setalternativa5(NULL);
        }
        if($this->getresolucao() == ''){
            $this->setresolucao(NULL);
        }
        
        if($this->getprova_id() ==""){
            
            $this->setprova_id(3);
        }
        
        if($this->getassunto_id() == ""){
            
            $this->setassunto_id(131);
        }
        
        $r = $sql->select("CALL sp_questions_save(:legenda_da_imagem, :img_url, :texto, :pergunta, :alternativa1, :alternativa2, :alternativa3, :alternativa4, :alternativa5, :correta, :resolucao, :prova_id, :assunto_id)", array(
            ":legenda_da_imagem"=>$this->getlegenda_da_imagem(),
            ":img_url"=>$this->getimg_url(),
            ":texto"=>$this->gettexto(),
            ":pergunta"=>$this->getpergunta(),
            ":alternativa1"=>$this->getalternativa1(),
            ":alternativa2"=>$this->getalternativa2(),
            ":alternativa3"=>$this->getalternativa3(),
            ":alternativa4"=>$this->getalternativa4(),
            ":alternativa5"=>$this->getalternativa5(),
            ":correta"=>$this->getcorreta(),
            ":resolucao"=>$this->getresolucao(),
            ":prova_id"=>$this->getprova_id(),
            ":assunto_id"=>$this->getassunto_id()
            
        ));
          
        $this->setData($r[0]);
        
        Materia::updateFile();
    }
    
    public function get($questao_id){ //Falta implementar
        
        $sql = new Sql();
        
        $results = $sql->select("SELECT tb_questao.*, tb_questao_assunto.assunto_id FROM tb_questao INNER JOIN tb_questao_assunto ON tb_questao_assunto.questao_id = tb_questao.id WHERE tb_questao.id = :idquestao LIMIT 1", array(
        
        ":idquestao"=>$questao_id
        
        ));
        
        $this->setData($results[0]);
        
    }
    
    
    public function update(){
        
        $sql = new Sql();
        
        /*
            legenda_da_imagem varchar(200), 
            img_url varchar(30),
            texto varchar(3000),
            pergunta varchar(1200),
            alternativa1 varchar(1000),
            alternativa2 varchar(1000),
            alternativa3 varchar(1000),
            alternativa4 varchar(1000),
            alternativa5 varchar(1000),
            correta varchar(200),
            resolucao varchar(3000) ,
            prova_id int(11),
            assunto_id INT
        */
        
        if($this->gettexto() == ""){
            $this->settexto(NULL);
        }
        
        if($this->getimg_url() == ''){
            $this->setimg_url(NULL);
        }

        if($this->getlegenda_da_imagem() == ''){
            $this->setlegenda_da_imagem(NULL);
        }

        if($this->getalternativa3() == ''){
            $this->setalternativa3(NULL);
        }
        if($this->getalternativa4() == ''){
            $this->setalternativa4(NULL);
        }

        if($this->getalternativa5() == ''){
            $this->setalternativa5(NULL);
        }
        if($this->getresolucao() == ''){
            $this->setresolucao(NULL);
        }
        
        if($this->getprova_id() ==""){
            
            $this->setprova_id(3);
        }
        
        if($this->getassunto_id() == ""){
            
            $this->setassunto_id(131);
        }
        
        $r = $sql->select("CALL sp_questionsupdate_save(:id, :legenda_da_imagem, :img_url, :texto, :pergunta, :alternativa1, :alternativa2, :alternativa3, :alternativa4, :alternativa5, :correta, :resolucao, :prova_id, :assunto_id)", array(
            ":id"=>$this->getid(),
            ":legenda_da_imagem"=>$this->getlegenda_da_imagem(),
            ":img_url"=>$this->getimg_url(),
            ":texto"=>$this->gettexto(),
            ":pergunta"=>$this->getpergunta(),
            ":alternativa1"=>$this->getalternativa1(),
            ":alternativa2"=>$this->getalternativa2(),
            ":alternativa3"=>$this->getalternativa3(),
            ":alternativa4"=>$this->getalternativa4(),
            ":alternativa5"=>$this->getalternativa5(),
            ":correta"=>$this->getcorreta(),
            ":resolucao"=>$this->getresolucao(),
            ":prova_id"=>$this->getprova_id(),
            ":assunto_id"=>$this->getassunto_id(),
            
        ));
          
        $this->setData($r[0]);
        
    }
    
    public function delete(){ //falta
        
        $sql = new Sql();
        
        $sql->query("CALL sp_questions_delete(:id)", array(
        
            ":id"=>$this->getid()
        
        ));
        
    }
}

 ?>