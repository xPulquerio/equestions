<?php 

namespace EQuestions\Model;

use \EQuestions\Model;
use \EQuestions\DB\Sql;

class Prova extends Model {

	protected $fields = [
		"id","cargo","nivel","orgao", "banca", "ano", "qtd_de_questoes", "banca_sigla", "orgao_sigla", "banca_id", "orgao_id"
	];

    
    public static function listAllforProva($prova_id){
        
        $sql = new Sql();
        
        return $sql->select("SELECT tb_questao.*, tb_assunto.nome as assunto FROM tb_questao INNER JOIN tb_questao_assunto on tb_questao.id = tb_questao_assunto.questao_id INNER JOIN tb_assunto ON tb_questao_assunto.assunto_id = tb_assunto.id WHERE tb_questao.prova_id = :prova_id GROUP BY tb_questao.id ORDER BY rand()",
                           array (
                           ":prova_id"=>$prova_id
                           ));
        
    }
    
    public static function listAllOnlyWithProvas(){
        
        $sql = new Sql();
        
        return $sql->select("SELECT * FROM vw_prova_banca_orgao ORDER BY Qtd Desc");
        
    }
    
    public static function listAll(){
        
        $sql = new Sql();
        
        return $sql->select("SELECT * FROM vw_allprova_banca_orgao ORDER BY cargo Asc");
        
    }
    
    
    public function save(){
        
        $sql = new Sql();
        
        /*
            pprova_id INT,
            pprova_ano YEAR(4),
            pprova_nivel enum('Superior', 'Médio', 'Fundamental'),
            pprova_cargo VARCHAR(200),
            pprova_banca_id INT,
            pprova_orgao_id INT
        */
    
        
        $r = $sql->select("CALL sp_provas_save(:id, :ano, :nivel, :cargo, :banca_id, :orgao_id)", array(
            ":id"=>$this->getid(),
            ":ano"=>$this->getano(),
            ":nivel"=>$this->getnivel(),
            ":cargo"=>$this->getcargo(),
            ":banca_id"=>$this->getbanca_id(),
            ":orgao_id"=>$this->getorgao_id()            
        ));
          
        $this->setData($r[0]);
    }
    
    public function get($prova_id){
        
        $sql = new Sql();
        
        $results = $sql->select("SELECT * FROM vw_allprova_banca_orgao WHERE id = :prova_id", array(
        
        ":prova_id"=>$prova_id
        
        ));
        
        $this->setData($results[0]);
        
    }
 
    public function delete(){
        
        $sql = new Sql();
        
        $sql->query("DELETE FROM tb_prova WHERE id = :id", array(
        
            ":id"=>$this->getid()
        
        ));
        
    }

}

 ?>